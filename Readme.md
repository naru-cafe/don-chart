# don chart

this is a fork of [mastodon's official chart](https://github.com/mastodon/mastodon/tree/main/chart)

- for purpose of running them in arm64
- in-source db & redis which uses official docker image, not [bitnami's](https://github.com/bitnami/charts/issues/7305)
- learning k8s
    - in-source db and redis are not capable of hight-availability yet

## how

1. with docker-compose, do the initial setup steps, generating `.env.production.yaml`
2. make values.private.yaml
3. helm install -f values.private.yaml chart (maybe? I'm gonna do this today)