#!/bin/bash
helm install \
    --namespace don-naru-cafe --create-namespace \
    don-rel \
    ../don-chart/chart -f ../don-chart-private/values.private.yaml \
    --debug --timeout 0
